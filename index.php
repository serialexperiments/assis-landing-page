<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="vendor/bootstrap-4.1.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <meta name="theme-color" content="#ffc107">
  <title>ASSIS</title>
</head>
<body>
  <header>
    <nav class="navbar navbar-dark bg-dark">
      <a class="navbar-brand" href="#">
      </a>
    </nav>
  </header>
  <div class="container-fluid con-main text-light bg-dark mt-4 pb-5">
    <div class="row d-flex justify-content-center pt-5">
      <h2>¿Qué buscas?</h2>
    </div>
    <div class="row py-5">
      <div id="colMinero" class="col-md-6 col-web d-flex align-items-center">
        <div class="image-overlay-minero">
          <div class="text-center">
            <img class="img-logo-minero" src="img/logoMinero.png" alt="">
            <p class="p-4">Especialistas en equipamiento minero, jaulas antivuelco. Carrocerías para transporte de explosivos. Fabricaciones especiales. Repuestos de camionetas y camiones.</p>
            <a href="#" class="btn btn-transparent">Ir al sitio</a>
          </div>
        </div>
      </div>
      <div id="colCustoms" class="col-md-6 col-web d-flex align-items-center">
        <div class="image-overlay-customs">
          <div class="text-center">
            <img class="img-logo-customs" src="img/logoCustoms.png" alt="">
            <p class="px-5 p-4">Servicio mecánico multimarca, scanner, reparación y borrado de fallas, aire acondicionado, alarmas, polarizado, barras antivuelco decorativas y mucho más.</p>
            <a href="customs.php" class="btn btn-transparent">Ir al sitio</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer class="container-fluid bg-dark text-light">
    <div class="row bar-footer"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 d-flex align-items-center justify-content-center pb-4">
          <a href="#">
            <i class="fab fa-facebook-f fa-2x color-primary"></i>
          </a>
          <a class="pl-4" href="#">
            <i class="fab fa-twitter fa-2x color-primary"></i>
          </a>
        </div>
        <div class="col-md-12 text-center">
          <address>
            <i class="fas fa-map-marker-alt color-primary mr-2"></i>
            Avenida Cachapoal 1179 25B, Rancagua.<br>
          </address>
        </div>
      </div>
    </div>
  </footer>
</body>
<script type="text/javascript" src="vendor/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="vendor/bootstrap-4.1.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="vendor/bootstrap-4.1.1/js/bootstrap.min.js"></script>

<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
</html>
