$(document).ready(() => {

  let alignClass = 'd-flex justify-content-center';
  let nav = document.getElementsByClassName('navSmooth');

  //Agrega o quita clases al menu de navegación
  if ($('body').width() > '992') {
    $('.collapse').addClass(alignClass);
  }

  $(window).resize(() => {
    if ($('body').width() > '992') {
      $('.collapse').addClass(alignClass);
    } else {
      $('.collapse').removeClass(alignClass);
    }
  })

  // Ajax para formulario de contacto
  $('#frmContacto').on('submit', (e) => {

    e.preventDefault();
    console.log($('#frmContacto').serialize());

    $.ajax({
      url: 'mail.php',
      method: 'post',
      data: $('#frmContacto').serialize(),
      success: (response) => {
        console.log(response);
      }
    });
  });

  //Añade animaciones al hacer scroll
  function smoothScroll (e){

    e.preventDefault();

    let navTo = this.dataset.navto;

    document.getElementById(navTo).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  }

  for (var i = 0; i < nav.length; i++) {
    nav[i].addEventListener('click', smoothScroll);
  }
});
