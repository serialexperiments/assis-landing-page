<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="vendor/bootstrap-4.1.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/customs.css">
  <meta name="theme-color" content="#dbbb00">
  <title>ASSIS Customs</title>
</head>
<body class="bg-light">
  <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark d-flex align-items-center justify-content-center">

      <a class="navbar-brand d-block d-sm-none" href="#">
        <img class="img-logo-customs" src="img/logoCustoms.png" alt="">
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav text-center">
          <li class="nav-item d-flex align-items-center justify-content-center">
            <a class="nav-link px-3 navSmooth" data-navto="con-servicios" href="#">SERVICIOS</a>
          </li>
          <li class="nav-item d-flex align-items-center justify-content-center">
            <a class="nav-link px-3 navSmooth" data-navto="con-ubicacion" href="#">UBICACIÓN</a>
          </li>
          <a class="navbar-brand mx-auto d-none d-sm-block" href="#">
            <img class="img-logo-customs px-3" src="img/logoCustoms.png" alt="">
          </a>
          <li class="nav-item d-flex align-items-center justify-content-center">
            <a class="nav-link px-3 navSmooth" data-navto="con-contacto" href="#">CONTACTO</a>
          </li>
          <li class="nav-item d-flex align-items-center justify-content-center">
            <a class="nav-link px-3" href="galeria.php">GALERÍA</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
    <!--ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol-->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="img/1.png" alt="First slide">
      </div>
      <!--div class="carousel-item">
        <img class="d-block w-100" src="img/1.png" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/1.png" alt="Third slide">
      </div-->
    </div>
    <!--a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Siguiente</span>
    </a>
  </div-->
  <div id="con-servicios" class="container text-center pb-3">
    <h2 class="pt-5 pb-5">Nuestros servicios</h2>

    <div class="row">
      <div class="col-md-3 pb-5">
        <div class="rounded-circle p-5 text-white bg-warning rounded d-flex align-items-center justify-content-center">
          <img class="svg-icon" src="img/icon/hooter.svg" alt="">
        </div>
        <h4 class="pt-4 font-weight-bold">Alarmas</h4>
      </div>
      <div class="col-md-3 pb-5">
        <div class="rounded-circle p-5 text-white bg-warning rounded d-flex align-items-center justify-content-center">
          <img class="svg-icon" src="img/icon/repair.svg" alt="">
        </div>
        <h4 class="pt-4 font-weight-bold">Servicio mecánico multimarca</h4>
      </div>
      <div class="col-md-3 pb-5">
        <div class="rounded-circle p-5 text-white bg-warning rounded d-flex align-items-center justify-content-center">
          <img class="svg-icon" src="img/icon/repairing.svg" alt="">
        </div>
        <h4 class="pt-4 font-weight-bold">Reparación y borrado de fallas</h4>
      </div>
      <div class="col-md-3 pb-5">
        <div class="rounded-circle p-5 text-white bg-warning rounded d-flex align-items-center justify-content-center">
          <img class="svg-icon" src="img/icon/voltmeter.svg" alt="">
        </div>
        <h4 class="pt-4 font-weight-bold">Scanner</h4>
      </div>
      <div class="col-md-3 pb-5">
        <div class="rounded-circle p-5 text-white bg-warning rounded d-flex align-items-center justify-content-center">
          <img class="svg-icon" src="img/icon/car-door.svg" alt="">
        </div>
        <h4 class="pt-4 font-weight-bold">Polarizado</h4>
      </div>
      <div class="col-md-3 pb-5">
        <div class="rounded-circle p-5 text-white bg-warning rounded d-flex align-items-center justify-content-center">
          <img class="svg-icon" src="img/icon/air-conditioner.svg" alt="">
        </div>
        <h4 class="pt-4 font-weight-bold">Aire acondicionado</h4>
      </div>
      <div class="col-md-3 pb-5">
        <div class="rounded-circle p-5 text-white bg-warning rounded d-flex align-items-center justify-content-center">
          <img class="svg-icon" src="img/icon/cone.svg" alt="">
        </div>
        <h4 class="pt-4 font-weight-bold">Barras antivuelco</h4>
      </div>
      <div class="col-md-3 pb-5">
        <div class="rounded-circle p-5 text-white bg-warning rounded d-flex align-items-center justify-content-center">
          <img class="svg-icon" src="img/icon/toolbox.svg" alt="">
        </div>
        <h4 class="pt-4 font-weight-bold">¡Y mucho mas!</h4>
      </div>
    </div>
  </div>
  <div id="con-ubicacion" class="container-fluid text-center pb-3">
    <h2 class="pb-4">Encuentranos en</h2>
    <div class="row">
        <iframe class="w-100" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13201.780445250592!2d-70.748555!3d-34.186102!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x77f371e63f7aebba!2sASSIS+SEGURIDAD!5e0!3m2!1ses-419!2scl!4v1527575225991" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div>
  <div id="con-contacto" class="container pb-5">
    <div class="row">
      <div class="col-md-12">
        <h2 class="py-4 text-center">Contacto</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <form id="frmContacto">
          <fieldset>
            <div class="form-row">
              <div class="col-md-6">
                <label for="">Nombre:</label>
                <input class="form-control" type="text" name="txtName" required>
              </div>
              <div class="col-md-6">
                <label for="">Correo electrónico:</label>
                <input class="form-control" type="mail" name="txtMail" required>
              </div>
            </div>
            <div class="form-row mt-2">
              <div class="col">
                <label for="">Mensaje:</label>
                <textarea class="form-control" name="txtMessage" rows="8" required></textarea>
              </div>
            </div>
            <input class="btn btn-block btn-danger float-right mt-3" type="submit" value="Enviar">
          </fieldset>
        </form>
      </div>
    </div>
  </div>
  <footer class="container-fluid bg-dark text-light">
    <div class="row bar-footer"></div>
    <div class="container pt-5 pb-4">
      <div class="row">
        <div class="col-md-4 d-flex align-items-center justify-content-center">
          <img class="img-logo pb-4" src="img/logo.png" alt="">
        </div>
        <div class="col-md-4 d-flex text-center align-items-center justify-content-center">
          <address>
            <strong class="color-primary">ASSIS EQUIPAMIENTO</strong>
            <br>
            Avenida Cachapoal 1179 25B <br>
            +56722768102 <br>
            ventas2@seguridadrancagua.cl
          </address>
        </div>
        <div class="col-md-4 d-flex align-items-center justify-content-center">
          <a href="#">
            <i class="fab fa-facebook-f fa-2x color-primary"></i>
          </a>
          <a class="pl-4" href="#">
            <i class="fab fa-twitter fa-2x color-primary"></i>
          </a>
        </div>
      </div>
    </div>
  </footer>
</body>
<script src="vendor/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script src="vendor/bootstrap-4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="vendor/bootstrap-4.1.1/js/bootstrap.min.js"></script>
<script src="js/customs.js"></script>

<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
</html>
