<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="vendor/bootstrap-4.1.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/customs.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
  <link rel="stylesheet" href="css/compact-gallery.css">
  <meta name="theme-color" content="#dbbb00">
  <title>ASSIS Customs</title>
</head>
<body class="bg-light">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark d-flex align-items-center justify-content-center">

    <a class="navbar-brand d-block d-sm-none" href="#">
      <img class="img-logo-customs" src="img/logoCustoms.png" alt="">
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav text-center">
        <li class="nav-item d-flex align-items-center justify-content-center">
          <a class="nav-link px-3" href="customs.php#con-servicios">SERVICIOS</a>
        </li>
        <li class="nav-item d-flex align-items-center justify-content-center">
          <a class="nav-link px-3" href="customs.php#con-ubicacion">UBICACIÓN</a>
        </li>
        <a class="navbar-brand mx-auto d-none d-sm-block" href="customs.php">
          <img class="img-logo-customs px-3" src="img/logoCustoms.png" alt="">
        </a>
        <li class="nav-item d-flex align-items-center justify-content-center">
          <a class="nav-link px-3" href="customs.php#con-contacto">CONTACTO</a>
        </li>
        <li class="nav-item d-flex align-items-center justify-content-center">
          <a class="nav-link px-3" href="#">GALERÍA</a>
        </li>
      </ul>
    </div>
  </nav>

  <section class="gallery-block compact-gallery py-4">
    <div class="container">
      <h2 class="text-center">Galería</h2>
      <hr>
      <div class="row no-gutters">
        <div class="col-md-6 col-lg-4 item zoom-on-hover">
            <a class="lightbox" href="img/gallery/g1.png">
                <img class="img-fluid image" src="img/gallery/g1.png">
                <span class="description">
                    <span class="description-heading"></span>
                    <span class="description-body"></span>
                </span>
            </a>
        </div>
        <div class="col-md-6 col-lg-4 item zoom-on-hover">
          <a class="lightbox" href="img/gallery/g2.png">
            <img class="img-fluid image" src="img/gallery/g2.png">
            <span class="description">
                <span class="description-heading"></span>
                <span class="description-body"></span>
            </span>
          </a>
        </div>
        <div class="col-md-6 col-lg-4 item zoom-on-hover">
          <a class="lightbox" href="img/gallery/g3.png">
            <img class="img-fluid image" src="img/gallery/g3.png">
            <span class="description">
                <span class="description-heading"></span>
                <span class="description-body"></span>
            </span>
          </a>
        </div>
        <div class="col-md-6 col-lg-4 item zoom-on-hover">
          <a class="lightbox" href="img/gallery/g4.png">
            <img class="img-fluid image" src="img/gallery/g4.png">
            <span class="description">
                <span class="description-heading"></span>
                <span class="description-body"></span>
            </span>
          </a>
        </div>
        <div class="col-md-6 col-lg-4 item zoom-on-hover">
          <a class="lightbox" href="img/gallery/g6.png">
            <img class="img-fluid image" src="img/gallery/g6.png">
            <span class="description">
                <span class="description-heading"></span>
                <span class="description-body"></span>
            </span>
          </a>
        </div>
        <div class="col-md-6 col-lg-4 item zoom-on-hover">
          <a class="lightbox" href="img/gallery/g7.png">
            <img class="img-fluid image" src="img/gallery/g7.png">
            <span class="description">
                <span class="description-heading"></span>
                <span class="description-body"></span>
            </span>
          </a>
        </div>
        <div class="col-md-6 col-lg-4 item zoom-on-hover">
          <a class="lightbox" href="img/gallery/g8.png">
            <img class="img-fluid image" src="img/gallery/g8.png">
            <span class="description">
                <span class="description-heading"></span>
                <span class="description-body"></span>
            </span>
          </a>
        </div>
        <div class="col-md-6 col-lg-4 item zoom-on-hover">
          <a class="lightbox" href="img/gallery/g5.jpg">
            <img class="img-fluid image" src="img/gallery/g5.jpg">
            <span class="description">
                <span class="description-heading"></span>
                <span class="description-body"></span>
            </span>
          </a>
        </div>
      </div>
    </div>
  </section>

  <div class="container-fluid bg-dark text-light">
    <div class="row bar-footer"></div>
    <div class="container pt-5 pb-4">
      <div class="row">
        <div class="col-md-4 d-flex align-items-center justify-content-center">
          <img class="img-logo pb-4" src="img/logo.png" alt="">
        </div>
        <div class="col-md-4 text-center d-flex align-items-center justify-content-center">
          <address>
            <strong class="color-primary">ASSIS EQUIPAMIENTO</strong>
            <br>
            Avenida Cachapoal 1179 25b <br>
            +56722768102 <br>
            ventas2@seguridadrancagua.cl
          </address>
        </div>
        <div class="col-md-4 d-flex align-items-center justify-content-center">
          <a href="#">
            <i class="fab fa-facebook-f fa-2x color-primary"></i>
          </a>
          <a class="pl-4" href="#">
            <i class="fab fa-twitter fa-2x color-primary"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="vendor/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script src="vendor/bootstrap-4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="vendor/bootstrap-4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="js/customs.js"></script>
<script src="js/galeria.js"></script>

<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
</html>
